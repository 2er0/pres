# pres

Some GitPitch peresentations made by 2er0

# Hold presentations

- [TensorFlow and basics to machine learning](https://gitlab.com/2er0/pres/tree/fhLUG-TF): Talk + Workshop
- [What is wrong with ML](https://gitlab.com/2er0/pres/tree/GranitTreff-ML): Talk
- [Plots with R everywhere](https://gitlab.com/2er0/pres/tree/1st-vhlug): Lightning Talk
